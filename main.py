from time import sleep
import webbrowser
from pytube import Playlist, YouTube


def youtube_audio_download(video_url):
    print(f"Opening download page for {YouTube(video_url).title}")
    webbrowser.open(f"https://apiyoutube.cc/?url={video_url}")


def download_from_playlist(playlist_url):
    # Retrieve URLs of videos from playlist
    playlist = Playlist(playlist_url)
    print(f"Now downloading {playlist.title}")

    for url in playlist.video_urls:
        input("Press enter for next download. ")
        youtube_audio_download(url)


def download_from_playlists(*playlist_urls):
    for url in playlist_urls:
        download_from_playlist(url)


if __name__ == '__main__':
    while(True):
        user_input = input("Paste here the playlist or video URL. Separate multiple URLs with a comma(,). "
                           "Spaces and newlines will be ignored. Enter Q to quit.\n")
        if user_input.lower() == "q":
            print("Goodbye...")
            sleep(0.5)
            quit()

        user_urls = user_input.replace(" ", "").replace("\n", "").split(",")

        for user_url in user_urls:
            if "playlist" in user_url:
                download_from_playlist(user_url)
            elif "watch" in user_url:
                youtube_audio_download(user_url)
